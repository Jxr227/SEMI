# Scripting Engine for Melvor Idle

### **SEMI** is an **UNOFFICIAL** Firefox and Chrome web extension for Melvor Idle.

**As with any scripts or game mods, please back up your data before making any changes!**

[![Mozilla Firefox](https://img.shields.io/amo/v/scripting-engine-melvor-idle?label=Get%20SEMI%20for%20Firefox&logo=firefox)](https://addons.mozilla.org/en-US/firefox/addon/scripting-engine-melvor-idle/)
[![Google Chrome](https://img.shields.io/chrome-web-store/v/mnjfmmpkdmgfpabgbeoclagnclmpmjgm?label=Get%20SEMI%20for%20Chrome&logo=Google%20Chrome)](https://chrome.google.com/webstore/detail/scripting-engine-for-melv/mnjfmmpkdmgfpabgbeoclagnclmpmjgm?authuser=0&hl=en)

Chrome updates can be one or two days behind sometimes due to slow review. Buttons above reflect currently available versions on the official platforms.

New: you are able to download pre-built releases of SEMI in a .zip file, handy for manual installation in your browser or installation into Steam. Go to the [SEMI releases page](https://gitlab.com/aldousWatts/SEMI/-/releases) and click on the 'build' link under 'Others' to download your zip, and see [Alternative Installation](#alternative-installation) below.

### Contributors

Huge thanks to DanielRX, Visua, Breindahl, AuroraKy, Zeldo, Parataku, Alex Mauney, Shamus Taylor, TheAlpacalypse, and many more for helping with the project!

## What can SEMI do?

This add-on/extension helps you automate certain aspects of the game, and adds certain useful features, combining many scripts into one. Toggle each one on and off individually from inside the game. Hovering over the SEMI sidebar buttons will give tooltips including hints, explanations, and tips for most scripts.

* New **Beta** feature: Drop chances on monster loot modals by TheAlpacalypse!
* New **Beta** script: Save On Close by TheAlpacalypse!
    * Prompts you to save your game to cloud when you leave the page.
* New **Beta** script: AutoRunecraft by TheAlpacalypse!
    * AutoRunecraft will automatically create runes based on the ratios set up by you.
    * Will mine Rune Essence if you run out
    * Will integrate with a future plugin to automatically set up ratios based on current spell setup
* New: AutoLute by Ugh!
    * AutoLute monitors your combat opponent's health and switches to Lute for the final kill of a dungeon, or the killing blow for normal combat, for the 5x GP reward.
* Updated script: ETA replaces TimeRemaining
    * More recently maintained fork by TinyCoyote
    * Calculates estimated time to completion of your currently selected craft/production skill item, displaying in-menu
    * Also has a task-done alert, when a production skill completes all items and can't continue, a Task Complete alert will make an audible ding. Other dings available too!
    * It is more configurable than TimeRemaining, you can use the console to change parameters. [Check the script's code here to see what you can change.](https://greasyfork.org/en/scripts/415592-melvor-eta/code)
        * To change ETA settings with its new GUI, you must have the Melvor Idle Combat Simulator Reloaded extension installed.
* New **Beta** script: Auto Mastery by Alex Mauney!
    * AutoMaster will automatically spend down mastery pools when they are above 95%. It will spend your mastery points on your lowest mastery item in the particular skill above 95%. Warning: at this stage it will constantly close your mastery spending page and may interrupt attempts to spend other Mastery XP than what it is automating. Also, be aware that it will be affected by the Mastery XP Spending Multiplier buttons (+1, +5, +10)
* Mastery Enhancements Script by Acrone#1563, Aurora Aquir#4272, Breindahl#2660, NotCorgan#1234 and Visua#9999
    * Adds progress bars for pools to skills in the menu
* New **Beta** scripts by Zeldo:
    * Offline Time >12h [temporarily disabled]
        * Extends amount of time you can offline-idle
        * Cleans up offline idle calculations
    * AutoEquip Best Item
        * Equips best item for a situation
* New Core Features
    * Scripts can now include a config menu built-in to the sidebar using tippy, with AutoSellGems and AutoEquip Ammo being the prototype examples
    * Per-character config saving by Zeldo!
    * Event bus to intercept game events
    * Better sidebar menu statuses
    * Backup, Restore, and Reset SEMI Configuration in the repurposed SEMI Menu
    * AutoEnable Scripts on Refresh now available in the SEMI Menu
    * Toggle non-sidebar SEMI GUI elements
* AutoFarm by Visua
    * Replaces AutoReplant with a much more customizable farming robot, with detailed settings like AutoMine.
    * Automatically harvests and then plants seeds, buying and using compost as needed, using seeds from your bank. Be sure you have enough seeds!
    * Automatically adds Weird Gloop if you have any in the bank. Prioritizes gloop over compost. Will not buy gloop
    * Optionally: If not in combat, equips Farming Cape/Signet Ring/Bob's Rake if you have them before harvesting and replanting
* Show Ore in Bank: adds ore counts to mining page rocks.
* AutoBonfire: continuously starts bonfires for you when you have a type of wood selected in Firemaking
* AutoCook: cycles through fish and cooks them all
* AutoMine
    * v0.4: New and improved GUI for setting the mineArray priorities by dragging elements on the page
    * Mine-by-ratios by selecting a bar to mine for
    * v0.4: New option to directly modify coal ratios when mining for bars
* AutoSellGems: sells 100 gems once they've reached a stack of 100
* AutoSmith Bars: cycle through smithing bars and smelt them if you have the materials to do so
* Katorone Automation & GUI
    * In-game menu for toggling & using certain Katorone automation script functions, which still does these things:
        * Set a GP amount to keep in reserve
        * Automatically buys more bank space when full
        * Automatically buys Gem Glove Charges, selling gems to acquire money for more charges or bank space
        * Now saves your custom configuration settings, persisting across refreshes
* AutoSell, AutoBury, and AutoOpen
    * GUI menus for selecting items you want to sell, bury, or open automatically. Selections saved in SEMI localstorage config
* Combat Scripts
    * AutoSlayer
        * Automates Slayer tasks, entering combat and constantly engaging whatever monster you're assigned
        * Optionally automatically equips mirror shield or magic ring, replacing original equipment when done
        * Optionally skips monsters based on a GUI selection menu similar to AutoSell
    * AutoRun: Exit combat if you're out of food/ammo/runes or if enemy can one-hit-kill you
    * AutoEquip Ammo: Attempts to equip more of the same type of arrow if using ranged before exiting combat. Now works in dungeons if you have dungeon equipment swap purchased
    * AutoEat
        * Eats when HP is less than what food heals, or when HP is less than max hit of your enemy
        * Usable for thieving as well as combat
        * Automatically cycles to next equipped food if you run out
        * Takes damage reduction, stun damage increases, burning damage, and damage reflection into consideration for max hit calculations
        * Tested for god dungeons
    * AutoLoot: picks up dropped loot items for you
* Extra functions & buttons
    * Toggle "OTHER" & "SOCIALS" Sidebar Button section visibility like you can do for "COMBAT" & "SKILLS"
    * Drag-able sidebar menu items by DanielRX!
        * Optional hidden section. Items dragged under the semi-icon divider will be hidden when the order is locked!
        * Order saved in SEMI localstorage
    * Destroy All Crops button in the Farming page
    * Barf My Potion button in the Potion selection menu
    * Thieving calculators and tooltips by RedSparr0w (scavenged from Melvor Idle Helper)
    * XPH script by Breakit, now with an in-game GUI to display XP per hour and estimate time until reaching the level of your choice
    * Many code utilities that are useful in scripting for Melvor Idle. See `utils.js`

### Bugs & Requests

Notice a bug? Have an idea for something SEMI can do?

You can post [here on the issues page of the SEMI repository](https://gitlab.com/aldousWatts/SEMI/-/issues), chat about it in the Melvor discord in the #scripting-and-extensions channel, and/or talk to Aldous Watts.

## Alternative Installation

Download a [SEMI release build zip](https://gitlab.com/aldousWatts/SEMI/-/releases), or clone the repo and build from source using `npm run build` (check your `.build` directory for zip). **Beware:** This installation method will never automatically update, and if you build from dev branch yourself, the build may be unstable.

### Chrome Developer Install

Unzip/decompress your SEMI build zip to a new folder.

In Chrome, open settings > tools > extensions, or go to this url without quotes: "chrome://extensions".

Enable developer options in the top right, then click "Load Unpacked" on the left.

Open the folder that contains manifest.json to load the add-on.

### Firefox Developer Install

Go to your addons page: Open the url "about:addons" without quotes, OR hit Ctrl+Shift+A, OR click the top-right hamburger settings menu button and click "Add-ons and themes".

On newest firefox, click the gear in the top-right corner under the search bar of the addons page.

Install the addon by clicking "Install Add-on From File..." and opening the zip.

## Goal of the Software

This software was made to unify many Melvor automation and QOL scripts, including my own, into one easy-to-use platform with a UI that mirrors the game, without worrying about compatibility or maintaining individual userscripts.

# Development & Building From Source

SEMI has become much more complex over time. It now has prerequisite software you will need for development and building the extension locally, but has more convenient features now as well. You will likely need to be familiar with git, console commands using a terminal, a bit of typescript, and node.

My hope is that coders or developers who aren't up to speed on node or ts get an idea of what's going on using this section. Hopefully that means more people will be able to contribute despite the new changes, and maybe learn some things along the way.

## Node & NPM

You will need node and node package manager, recommend at least 12.x LTS. NPM comes with node.

Get them here: https://nodejs.org/

After you have node and npm installed, clone this repository and open your terminal in the cloned project directory. This next step may take a minute or two to install many packages, which can take up about 350MB of space on your drive. Run the following command to install node packages that the project depends on.

`npm install`

(or just `npm i` ).

After it is done installing, you can build from source using this terminal command inside the project directory:

`npm run build`

This tells node to run the build script you see in the root of the project: `build.js`. This will properly package files needed to run the extension both in `./dist` as unzipped javascript, and in `./.build/` as a pre-built zip file.

## TypeScript

The project source is now TypeScript. This is a superset of javascript that allows for types, which is handy for preventing specific bugs, and better intellisense, among other benefits. However, any valid js is also valid ts, so the barrier of entry is not too great.

Read up on ts here: https://www.typescriptlang.org/

In order to change the source, open and change any `.ts` file inside the `./src` project directory. **Don't panic!** There are lots of typescript errors right now as we are still working on fully converting our source to typescript.

To convert your typescript into something browsers can use, you need to transpile ts into js. Do so by either installing the TypeScript package globally with npm (so you can run `tsc` from your terminal), or just use `npx` that is included in npm to run it (example: `npx tsc`). Running `tsc` transpiles the project code using the `tsconfig.json` and node packages in the project. Our project is set to output its code to the `./dist` project directory, but simply running tsc is not enough to fully build the project since it will not copy `manifest.json` into `./dist` like `build.js` will.

So, again, if you want to actually build the code and not just transpile with `tsc`, use `npm run build`

## VS Code

This project is developed by and for VS Code, a free and open source text-editor/extensible-IDE that has a ton of great features.

Find it here: https://code.visualstudio.com/

We recommend and apply the Prettier VS Code extension by default upon saving a file in VS Code. Prettier is a code formatter that keeps code clean, readable, and a little less prone to merge conflicts.

There are launch and build tasks for VS Code included in the repository to improve the development experience! See `./.vscode/`

### Debugging

We have debugging tasks for live debugging and running the extension in an isolated browser environment.

**Prerequisites**: Firefox and Chrome debugging VS Code extensions.

Open your Run and Debug panel on the left side of VS Code and select an option up top. I recommend Launch FF, which will run a build task then launch the extension in a new, development-profile window of FF that will automatically open Melvor for you. Click the green arrow to start. Make changes to the typescript src, then hit the "Reload" button in VS Code (green refresh icon) to rebuild and refresh the page so your changes appear.

## Thanks Again

This project has been maintained primarily by the community. Thanks to all the helpful developers out there! And thanks to all the users, it's all for you!
